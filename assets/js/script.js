let btn = document.querySelector('#btn-submit');
let choices = document.querySelector('#choices');
let scoreContainer = document.querySelector('#score');
let questionContainer = document.querySelector('#question');
let score = [];
let nameOfTaker = document.querySelector("#nameOfTaker").value;

const questionBank = [
  {
    question: 'Who is the current president of the republic of the philippines?',
    choices: {
      1: "Rodrigo Rudy Duterte",
      2: "Rodney Roa Duterte",
      3: "Rodrigo Roa Duterte",
      4: "none of the above"
    },
    answer: 3
  },
  {
    question: 'What is the capital of the philippines?',
    choices: {
      1: "Zamboanga",
      2: "Metro Cebu",
      3: "Metro Manila",
      4: "all of the above"
    },
    answer: 3
  },
  {
    question: 'How many stars does the philippine flag have?',
    choices: {
      1: "4",
      2: "3",
      3: "7",
      4: 'none'
    },
    answer: 2
  },
  {
  	question: 'What is PAGASA stands for?',
    choices: {
      1: "Pinoy Atmospheric, Geophysical and Astronomical Services Administration ",
      2: "Philippine Atmospheric, Geophysical and Astronomical Services Administration",
      3: " Philippine Atmospheric, Geographical and Astronomical Services Administration ",
      4: 'Philippine Atmospheric, Geophysical and Astronomical Services Association'
    },
    answer: 2

  }

];

btn.addEventListener('click', function() {
  //call findItem function and pass in the length of the score array which is in getScore function
  
  document.querySelector("#btn-submit").textContent = "Next";

  findItem(getScore());

  //find the input element with the attribute name "option" that is checked
  let answer = document.querySelector('input[name="option"]:checked');

  //if it exists, call checkAnswer function and pass in two arguments: the length of the score array and the answer element's VALUE (Note: answer element is an input element)
  if (answer) {
    checkAnswer(getScore(), answer.value);
  }


});


function getScore() {
  //return the length of the score array
  return score.length;
}

function findItem(indexPosition) {
  try {
    //find the item in the questionBank array through its index position
    let item = questionBank[indexPosition];
    //if it exists, call createItem function and pass in the item that you have found
    createItem(item);
  } catch (e) {
    //if e instance of TypeError
    if (e instanceof TypeError) {
      // use reducer method to add the values inside score array and get your final score; see documentation
      // display final score and message inside questionContainer element; see line 4
      const reducer = (accumulator, currentValue) => accumulator + currentValue;
      let finalScore = score.reduce(reducer);
      scoreContainer.innerHTML = finalScore + ' out of ' + getScore();
      questionContainer.innerHTML = '<h6>Your final score</h6>';
    }
  }
}

function createNewElement(element, text) {
  //create element and pass in the element parameter
  let newElement = document.createElement(element);
  //add text content to the created element and pass in text parameter
  newElement.textContent = text;
  //return created element
  return newElement;
}

function createChoice(value) {
  //call createElement function to create input element
  let input = createNewElement('input');
  /*
    set attributes for input so that it will produce the following result
    <input class="form-check-input position-static mr-3" type="checkbox" name="option" value="value">

    Note that you need to pass in the value parameter when setting attribute for value
  */
  input.setAttribute('class', 'form-check-input position-static mr-3');
  input.setAttribute('type', 'checkbox');
  input.setAttribute('name', 'option');
  input.setAttribute('value', value);
  //return input
  return input;
}

function createItem(item) {
  //assign item.question to questionContainer's inner text
  questionContainer.innerText = item.question;
  let formCheck;
  let label;
  let span;
  //create a loop. 4 because you have 4 choices
  for (let x = 1; x <= 4; x++) {
    /* 
      call createNewElement function to create div, label, and span
      call createChoice function an pass in x as value
      set appropriate attributes and do appropraite appendChild method to create the following result:

      <div class="form-check">
          <label>
            <input class="form-check-input position-static mr-3" type="checkbox" name="option" value="1">
            <span>option 1</span>
          </label>
      </div>
    */
    formCheck = createNewElement('div');
    formCheck.setAttribute('class', 'form-check');
    label = createNewElement('label');
    input = createChoice(x);
    span = createNewElement('span', item.choices[x]);
    label.appendChild(input);
    label.appendChild(span);
    formCheck.appendChild(label);
    choices.appendChild(formCheck);
  }

  //return choices
  return choices;
}

function checkAnswer(indexPosition, answer) {
  //create correctAnswer variable and assign it to the answer property in questionBank array
  let correctAnswer = questionBank[indexPosition].answer;
  //if the correctAnswer is equal to the answer
  if (correctAnswer == answer) {
    //push 1 to score array
    score.push(1);
  } else {
    //push 0 to score array
    score.push(0);
  }
  //remove content inside choices by assigning it's innerHTML to a blank string
  choices.innerHTML = '';
  //call findItem function and pass in the length of the score array
  findItem(getScore());
}
